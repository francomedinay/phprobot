$(document).ready(function() {

    tableList();

});

function tableList() {
    $.ajax({
        url: 'table.php',
        type: 'GET',
        success: function(response) {
            console.log(response);
            let tasks = JSON.parse(response);
            let template = '';
            tasks.forEach(e => {
                template +=
              `<tr eleId="${e.ID}" eleP="${e.NOMBRE}" >
               <th scope="row">${e.ID}</th>
               <td id="nombre-user">${e.NOMBRE}</td>
               <td id="pass-user">${e.PASSWORD}</td>
               <td>${e.FECHA}</td>
               <td><button type="button" class="btn-M btn btn-warning">Modifica</button> <button type="button" class="btn-E btn btn-danger">Elimina</button></td>         
               </tr>`
            });
            $('#tablacontenido').html(template);
        }
    });
}

$(document).on('click', '.btn-M', function() {
    let elemento1 = $(this)[0].parentElement.parentElement;
    let elemento2 = $(this)[0].parentElement.parentElement;
    let id = $(elemento1).attr('eleId');
    let name = document.getElementById("nombre-user");
    let pass = document.getElementById("pass-user");
    console.log(elemento1);
    console.log(elemento2);

    $.post('modificar.php', { name, pass,id  }, function(responde) {
        tableList();
        alert('Estas seguro modificar ?');
    });
});
$(document).on('click', '.btn-E', function() {
    let elemento1 = $(this)[0].parentElement.parentElement;
    let elemento2 = $(this)[0].parentElement.parentElement;
    let id = $(elemento1).attr('eleid');
    let nombre = $(elemento2).attr('elep');
    console.log(id);
    console.log(nombre);

    $.post('eliminar.php', { id, nombre}, function(responde) {
        tableList();
        alert('Estas seguro eliminar?');
    });
});