<?php 
require_once('dbutils.php');
$miConexion = conectarDB();
$datos2 = getInDatosFecha2($miConexion);
$totaldias = [];
$totalcantidad = [];
$totalsemana = [];
$semana = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
for ($i=0; $i <count($datos2) ; $i++) { 
        $totaldias[]=$datos2[$i]['dia'];

        $totalsemana[]=$semana[$i];
        $totalcantidad[]=$datos2[$i]['total'];
       
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script
  src="https://code.jquery.com/jquery-3.6.1.js"
  integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
  crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@latest/dist/Chart.min.js"></script>
    <title>Document</title>
    <style>
thead.heade-table {
  background-color: #7fffe6 !important;
}

#myChart{
  height: 50% !important;width: 50% !important
}
#imgb{
  height: 50% !important;width: 50% !important
}
</style>
</head>
<body>

<input type="hidden" name="semana" id="semana" value='<?php echo implode(',', $totaldias)?>'>

<?php 
    $img='<img alt="Embedded Image" src="" id="imgb" />';
    echo $img;
    echo '<input type="hidden" name="totalcantidad" id="totalcantidad" value='.implode(',', $totalcantidad).'>';
    echo '<input type="hidden" name="totalsemana" id="totalsemana" value='.implode(',', $totalsemana).'>';
    echo '<input type="hidden" name="obejctgraf" id="obejctgraf" value='.json_encode($datos2).'>';
?>

<canvas id="myChart" ></canvas>

<!-- <img alt="Embedded Image" src="" id="imgb" /> -->
 
<script src="script.js"></script>

</body>
</html>