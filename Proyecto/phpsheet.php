<?php 

include 'vendor/autoload.php';
require_once('dbutils.php');
$miConexion = conectarDB();
use PhpOffice\PhpSpreadsheet\{Spreadsheet,IOFactory};
/* use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx; */
use PhpOffice\PhpSpreadsheet\Writer\Xlsx; 
$datosT = getInformeDB($miConexion);

$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle('Informe');
$hojaActiva->setCellValue('A1','ID');
$hojaActiva->setCellValue('B1','USUARIO');
$hojaActiva->setCellValue('C1','AVERIA');
$hojaActiva->setCellValue('D1','FECHA_CREACION');
$hojaActiva->setCellValue('E1','ACCION_ROBOT');
$hojaActiva->setCellValue('F1','TIPOLOGIA');
$hojaActiva->setCellValue('G1','TECNOLOGIA');
$hojaActiva->setCellValue('H1','TIPO');
$hojaActiva->setCellValue('I1','FECHA_FIN');
$hojaActiva->setCellValue('J1','ROBOT');
$hojaActiva->setCellValue('K1','FECHA_ESCALADO');

$fila = 2;

/* foreach ($datosT as $key => $value) {
    
   echo $value['id'].'<br>';
} */
//foreach ($datosT as $key => $value) {
while($value = $datosT->fetch(PDO::FETCH_ASSOC)){  
$hojaActiva->setCellValue('A'.$fila,$value['id']);
$hojaActiva->setCellValue('B'.$fila,$value['usuario']); 
$hojaActiva->setCellValue('C'.$fila,$value['averia']); 
$hojaActiva->setCellValue('D'.$fila,$value['fecha_creacion']); 
$hojaActiva->setCellValue('E'.$fila,$value['accion_robot']); 
$hojaActiva->setCellValue('F'.$fila,$value['tipologia']); 
$hojaActiva->setCellValue('G'.$fila,$value['tecnologia']); 
$hojaActiva->setCellValue('H'.$fila,$value['fecha_inicio']); 
$hojaActiva->setCellValue('I'.$fila,$value['fecha_fin']); 
$hojaActiva->setCellValue('J'.$fila,$value['robot']); 
$hojaActiva->setCellValue('K'.$fila,$value['fecha_escalado']); 
$fila++;
//}
 }

/*  header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="informe.xls"');
header('Cache-Control: max-age=0'); */

$writer = IOFactory::createWriter($excel, 'Xls');
$writer->save('./informes/informe.xls');
/* header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="informe.xlsx"');
header('Cache-Control: max-age=0'); 

$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('./informes/informe.xlsx');
 */
/* $writer->save('php://output');
exit;  */

/* $spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'Hello World !');

$writer = new Xlsx($spreadsheet);
$writer->save('hello world.xlsx');
 */
?>