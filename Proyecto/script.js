var ctx = document.getElementById("myChart").getContext("2d");
var semana = document.getElementById("semana").value;
var totalcantidad = document.getElementById("totalcantidad").value;
var totalsemana = document.getElementById("totalsemana").value;
var obejctgraf = document.getElementById("obejctgraf").value
// Obtener una referencia al elemento canvas del DOM
const $grafica = document.querySelector("#grafica");
// Las etiquetas son las que van en el eje X. 
const etiquetas = totalsemana.split(',');
const datas = totalcantidad.split(',');
console.log(obejctgraf);

// Podemos tener varios conjuntos de datos. Comencemos con uno
const datosVentas2020 = {
    label: "Ventas por mes",
    data: datas, // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas
    backgroundColor: [
        "rgba(163,221,203,0.2)",
        "rgba(232,2331,161,0.2)",
        "rgba(230,18,102,0.2)",
        "rgba(229,112,126,0.2)",
        "rgba(229,112,126,0.2)",
        "rgba(229,112,126,0.2)",
        "rgba(229,112,126,0.2)",
    ], // Color de fondo
    borderColor: [
        "rgba(163,221,203,1)",
        "rgba(232,233,161,1)",
        "rgba(230,181,102,1)",
        "rgba(229,112,126,1)",
        "rgba(229,112,126,1)",
        "rgba(229,112,126,1)",
        "rgba(229,112,126,1)",
    ], // Color del borde
    borderWidth: 1,// Ancho del borde
};

var myChart = new Chart(ctx,{
//new Chart($grafica, {
    type: "bar",// Tipo de gráfica
    data: {
        labels: etiquetas,
        datasets: [
            datosVentas2020,
            // Aquí más datos...
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }],
        },
        responsive: true,
        animation: {
            onComplete: done
        }
    }
});

 function  done(){
    var url=myChart.toBase64Image();
    
    console.log(url);
    /* await sleep(5); 
    

    console.log(url);
    document.getElementById("imgb").src=url; */
    $(document).ready(function(){
        $.ajax({
            url: 'imggrafica.php',
                type: 'POST',
                data: {obj:JSON.parse(obejctgraf),id:url},
                success: function(){
                   /* alert("Ha sido ejecutada la acción."); */
                }
        });
});
    }


  var sleep = secs => new Promise(resolve => setTimeout(resolve, secs * 1000));
  
/* var image = myChart.toBase64Image();
document.getElementById("imgb").src=image */