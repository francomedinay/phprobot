<!DOCTYPE html>
<html lang="en">
<head>
<?php 

require_once('dbutils.php');
$miConexion = conectarDB();




?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.6.1.js"
  integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
  crossorigin="anonymous"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link rel="stylesheet" href="estilos.css">
<link rel="stylesheet" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
<!--link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css"-->
<script src="tabla.js"></script>

</head>
<body>
    
<?php 
include 'nav.php';

?>

<div class="container-welcome">
    <span class="name-welcome">Bienvenido Cristian</span>
</div>
<div class="container-description">
    <span class="name-description alert alert-primary" role="alert">Usuarios en la lista de espera porfavor recuerda que una vez iniciado no hay marcha atras</span><br><br><br>
</div>
<br>
<br>
<br>

<div class="tabla-usuarios">
<a target="_blank" type="button" class="btn btn-danger"  onclick="IniciarRobor()"><i class="fa-solid fa-check"></i>  Iniciar</a><br><br><br>

<table class="table">
  <thead>
    <tr>
      <th scope="col">Nº</th>
      <th scope="col">Nombre</th>
      <th scope="col">Password</th>
      <th scope="col">Hora</th>
      <th scope="col">Opcion</th>
    </tr>
  </thead>
  <tbody id="tablacontenido" border=1px>
 
  </tbody>
</table>
</div>

<script>
function IniciarRobor() {
//href="https://jira.masmovil.com/login.jsp"
Swal.fire({
icon: 'info',
title: 'Iniciar',
text: 'Iniciando Robot ... ',
footer: '<a href="">Porque no miras los registros?</a>',
showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Iniciar'
}).then((result) => {
/* Read more about isConfirmed, isDenied below */
if (result.isConfirmed) {
    
    window.location="https://jira.masmovil.com/login.jsp";
    Swal.fire('Eliminado !', '', 'success')
}  else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
  }


/* else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
} */
})

}

function eliminar() {

    Swal.fire({
  icon: 'error',
  title: 'Eliminar',
  text: 'Seguro que desea eliminar registro ?',
  footer: '<a href="">Porque no miras los registros ?</a>'
    }).then((result) => {
  /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
        Swal.fire('Eliminado !', '', 'success')
    } /* else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
    } */
})

}
function modificar() {

Swal.fire({
icon: 'info',
title: 'Modificar',
text: 'Seguro que desea modificar registro ?',
footer: '<a href="">Porque no miras los registros ?</a>'
}).then((result) => {
/* Read more about isConfirmed, isDenied below */
if (result.isConfirmed) {
    Swal.fire('Modificado !', '', 'success')
} /* else if (result.isDenied) {
    Swal.fire('Changes are not saved', '', 'info')
} */
})

}
</script>

</body>
</html>