<?php


function conectarDB(){
try{    
$cadenaConexion = "mysql:host=127.0.0.1:33067;dbname=zelenza";
$usuario="root";
$passW="";
$db = new PDO ($cadenaConexion,$usuario,$passW);
$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
return $db;

}
catch(PDOException $ex)
{
    echo "Error conectando".$ex->getMessage();
}
}
/* 


SELECT Date_format(`fecha_escalado`,'%Y-%m-%d') FROM `informes_robot`  
ORDER BY `informes_robot`.`fecha_escalado` DESC;
2022-12-15
SELECT CONCAT(ELT(WEEKDAY(Date_format(`fecha_escalado`,'%Y-%m-%d')) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) FROM `informes_robot`  
ORDER BY `informes_robot`.`fecha_escalado` DESC;

SELECT CONCAT(ELT(WEEKDAY(Date_format(`fecha_escalado`,'%Y-%m-%d')) + 1, '1', '2', '3', '4', '5', '6', '7')) FROM `informes_robot`  
ORDER BY `informes_robot`.`fecha_escalado` DESC;

SELECT WEEK(Date_format(`fecha_escalado`,'%Y-%m-%d'), 5) - WEEK(DATE_SUB(Date_format(`fecha_escalado`,'%Y-%m-%d'), INTERVAL DAYOFMONTH(Date_format(`fecha_escalado`,'%Y-%m-%d')) - 1 DAY), 5) + 1 FROM `informes_robot`  
ORDER BY `informes_robot`.`fecha_escalado` DESC; 



SELECT CONCAT(ELT(WEEKDAY(Date_format(NOW(),'%Y-%m-%d')) + 1, '1', '2', '3', '4', '5', '6', '7')),`fecha_escalado` FROM `informes_robot`WHERE   Date_format(NOW(),'%Y-%m-%d')= Date_format(`fecha_escalado`,'%Y-%m-%d')
ORDER BY `informes_robot`.`fecha_escalado` DESC;

SELECT * FROM `informes_robot`  where `fecha_escalado` BETWEEN date_add(NOW(), INTERVAL -2 DAY) AND NOW()
ORDER BY `informes_robot`.`fecha_escalado` DESC;
*/
function getUsuario($db) {
    $vectorTotal = array();
    try {
        $stmt = $db->query("SELECT * FROM usuarios");
        while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $vectorTotal[] = $fila;
        }
    } catch(PDOException $ex) {
        echo "Error getJuegos ".$ex->getMessage();
    }
    //$cad=json_encode($vectorTotal);
    return $vectorTotal;
}
function getDelay($db) {
    $vectorTotal = array();
    try {
        $stmt = $db->query("SELECT * FROM delay_aleatorio");
        while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $vectorTotal[] = $fila;
        }
    } catch(PDOException $ex) {
        echo "Error getJuegos ".$ex->getMessage();
    }
    //$cad=json_encode($vectorTotal);
    return $vectorTotal;
}

function getInformeDB($db) {
    
        $stmt = $db->query("SELECT * FROM informes_robot ORDER BY informes_robot.fecha_escalado DESC");
        return $stmt;
}


function getInDatosFecha($db) {
    $vectorTotal = array();
    $vectorTotal2 = array();
    $vectorTotal3 = array();
    
    $stmt = $db->query("SELECT CONCAT(ELT(WEEKDAY(Date_format(NOW(),'%Y-%m-%d')) + 1, '1', '2', '3', '4', '5', '6', '7')) as fecha ,fecha_escalado FROM informes_robot WHERE   Date_format(NOW(),'%Y-%m-%d')= Date_format(fecha_escalado,'%Y-%m-%d')
    ORDER BY informes_robot.fecha_escalado DESC");

    while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $vectorTotal[] = $fila;
    }
     $valor = array();
     $diasfecha = intval($vectorTotal[0]['fecha']);
    /* for ($i=1; $i <=intval($vectorTotal[0]['fecha']) ; $i++) { 
        $valor[]=$i;
    }  */
    //descontamos los dias
    
    //("SELECT fecha_escalado AS hoy FROM informes_robot  where fecha_escalado BETWEEN date_add(NOW(), INTERVAL -2 DAY) AND NOW() ORDER BY fecha_escalado DESC LIMIT 1");
    $stmt2 = $db->query("SELECT * FROM informes_robot  where fecha_escalado BETWEEN date_add(NOW(), INTERVAL -$diasfecha DAY) AND NOW() ORDER BY informes_robot.fecha_escalado DESC");
    while ($fila = $stmt2->fetch(PDO::FETCH_ASSOC)) {
        $vectorTotal2[] = $fila;
    }
    
    return $vectorTotal2;
}

function getInDatosFecha2($db) {
    $vectorTotal = array();
    $vectorTotal2 = array();
    $vectorTotal3 = array();
    
    $stmt = $db->query("SELECT CONCAT(ELT(WEEKDAY(Date_format(NOW(),'%Y-%m-%d')) + 1, '1', '2', '3', '4', '5', '6', '7')) as fecha ,fecha_escalado FROM informes_robot WHERE   Date_format(NOW(),'%Y-%m-%d')= Date_format(fecha_escalado,'%Y-%m-%d')
    ORDER BY informes_robot.fecha_escalado DESC");

    while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $vectorTotal[] = $fila;
    }
     $valor = array();
     $diasfecha = intval($vectorTotal[0]['fecha']);
    /* for ($i=1; $i <=intval($vectorTotal[0]['fecha']) ; $i++) { 
        $valor[]=$i;
    }  */
    //descontamos los dias
    
    $hoy='';
    for ($i=0; $i <$diasfecha ; $i++) { 
       
            $hoy = $db->query("SELECT DATE_SUB(Date_format(`fecha_escalado`,'%Y-%m-%d') , INTERVAL $i DAY) AS hoy FROM informes_robot  where fecha_escalado BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW() ORDER BY fecha_escalado DESC LIMIT 1");
        
            while ($fila = $hoy->fetch(PDO::FETCH_ASSOC)) {
                $vectorTotal3[] = $fila;
            }

           $now = $vectorTotal3[$i]['hoy'];
            
           $stmt2 = $db->query("SELECT COUNT(*) as total, Date_format(`fecha_escalado`,'%W') as dia FROM `informes_robot` where Date_format(`fecha_escalado`,'%Y-%m-%d')='$now' ORDER BY `informes_robot`.`fecha_escalado` DESC;"); 
           while ($fila = $stmt2->fetch(PDO::FETCH_ASSOC)) {
             $vectorTotal2[] = $fila;
           }
          
    }
    
    
    //("SELECT fecha_escalado AS hoy FROM informes_robot  where fecha_escalado BETWEEN date_add(NOW(), INTERVAL -2 DAY) AND NOW() ORDER BY fecha_escalado DESC LIMIT 1");
   
    
    
    //return $vectorTotal3[1]['hoy'];
    return array_reverse($vectorTotal2);
}

function insertarUsuario($db,$nombre,$password){
	//QUERY para obtener las iniciales a partir de el mail y se las pasas como parámetro al insert user
	//$datocorreo =insertarInicial($db,'econde@euro-funding.com');
	//$buscar	= "SELECT INICIALES FROM emailiniciales WHERE EMAIL=$email LIMIT 1";
	//$datocorreo =insertarInicial($db,$email);
	$sentenciaI = "INSERT INTO usuarios (NOMBRE, PASSWORD ) VALUES (:NOMBRE,:PASSWORD)";
    try{
    $stms = $db->prepare($sentenciaI);
    $stms->bindParam(":NOMBRE",$nombre);
    $stms->bindParam(":PASSWORD",$password);
    $stms->execute();       

    }catch(PDOException $ex){
        echo "ErrorNuevo".$ex->getMessage();
    }
    return $db-> lastInsertId();
}
function insertarRandDelay($db,$desde,$hasta,$tiempo){
	//QUERY para obtener las iniciales a partir de el mail y se las pasas como parámetro al insert user
	//$datocorreo =insertarInicial($db,'econde@euro-funding.com');
	//$buscar	= "SELECT INICIALES FROM emailiniciales WHERE EMAIL=$email LIMIT 1";
	//$datocorreo =insertarInicial($db,$email);
    $total = $desde.','.$hasta.','.$tiempo;
	$sentenciaI = "INSERT INTO delay_aleatorio (numero_aleatorio) VALUES (:numero_aleatorio)";
    try{
    $stms = $db->prepare($sentenciaI);
    $stms->bindParam(":numero_aleatorio",$total);
    $stms->execute();       
    }catch(PDOException $ex){
        echo "ErrorNuevo".$ex->getMessage();
    }
    return $db-> lastInsertId();

}

function actualizarDelay($db,$desde,$hasta,$tiempo,$id){

    $num_aleatorio=$desde.','.$hasta.','.$tiempo;
    $sentencia = "UPDATE delay_aleatorio SET numero_aleatorio='".$num_aleatorio."' WHERE ID='".$id."'";
    $stmt = $db->prepare($sentencia);
    $stmt->execute();
    $stmt->close();

}


function cambiarUsuario($db,$afternombre, $afterpassword, $id){    
    $stmt = $db->prepare("UPDATE usuarios SET NOMBRE =:NOMBRE, PASSWORD = :NOMBRE, WHERE ID=?");
    $stmt->bindParam(':nombre', $afternombre);
    $stmt->bindParam(':password', $afterpassword);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    /*   $stmt->bind_param('sss', $afternombre, $afterpassword, $id);
    
    if($stmt->execute()){
        return true;
        } else {
        return false;		
    } */
}

function EliminarUsuario($db,$nombre, $id){
		
    
    $stmt = $db->prepare("DELETE FROM usuarios WHERE nombre='".$nombre."' AND id='".$id."'");
    $stmt->bindParam(':nombre', $nombre);
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    
  /*   if($stmt->execute()){
        return true;
        } else {
        return false;		
    } */
}





function InsertarDatos($db,$usuario, $averia, $fecha_creacion, $accion_robot, $tipologia,$tecnologia,$fecha_inicio,$fecha_fin,$robot){                                                                                               
		
    
    $sentenciaI = "INSERT INTO informes_robot (usuario, averia ,fecha_creacion ,accion_robot ,tipologia ,tecnologia ,fecha_inicio, fecha_fin, robot ) VALUES (:usuario,:averia,:fecha_creacion,:accion_robot,:tipologia,:tecnologia,:fecha_inicio,:fecha_fin,:robot)";
    try{
        $stms = $db->prepare($sentenciaI);
        $stms->bindParam(":usuario",$usuario);
        $stms->bindParam(":averia",$averia);
        $stms->bindParam(":fecha_creacion",$fecha_creacion);
        $stms->bindParam(":accion_robot",$accion_robot);
        $stms->bindParam(":tipologia",$tipologia);
        $stms->bindParam(":tecnologia",$tecnologia);
        $stms->bindParam(":fecha_inicio",$fecha_inicio);
        $stms->bindParam(":fecha_fin",$fecha_fin);
        $stms->bindParam(":robot",$robot);
        $stms->execute();       
    
        }catch(PDOException $ex){
            echo "ErrorNuevo".$ex->getMessage();
        }
        return $db-> lastInsertId();
    
}



?>