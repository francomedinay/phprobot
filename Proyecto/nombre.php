<?php
	/* if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/dashboard/');
	exit; */
  header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
  header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
  header("Allow: GET, POST, OPTIONS, PUT, DELETE");


  $method =$_SERVER['REQUEST_METHOD'];
  if($method=="OPTIONS"){
    die();
  }
  use PhpOffice\PhpSpreadsheet\{Spreadsheet,IOFactory};
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx; 

  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use PHPMailer\PHPMailer\Exception;
  //Load Composer's autoloader
  /* require 'vendor/autoload.php'; */
  require 'PHPMailer/Exception.php';
  require 'PHPMailer/PHPMailer.php';
  require 'PHPMailer/SMTP.php';


echo dispatch();

//$dbConn =  connect($db);
/*
  listar todos los usuarios o solo uno
 */


function dispatch(){
  $data = json_decode(file_get_contents("php://input"), TRUE);
        

  $validas = array(
    "usuario" => 2,
    "usuarios"=>2,
    "informe_datos"=>2,
    "delayrand"=>2,
    "executephpexceladvh"=>2,
    "executephpexceladvh2"=>2,
    "executephpcita"=>2,
  );
 if(!$validas[$_REQUEST["o"]]) {
    header('HTTP/1.0 401 Unauthorized');
    exit;       
} 


$funcion = $_REQUEST["o"];
$o = $funcion($data);
return $o;
}

// Crear un nuevo post
function setTimeout($fn, $timeout){
  // sleep for $timeout milliseconds.
  sleep(($timeout/1000));
  $fn();
}


function informe_datos($data){
  
  include "bbdd.php";
  
  $usuario = json_encode($data["usuario"]);
  $averia = json_encode($data["averia"]);
  $fecha_creacion = json_encode($data["fecha_creacion"]);
  $accion_robot = json_encode($data["accion_robot"]);
  $tipologia = json_encode($data["tipologia"]);
  $tecnologia = json_encode($data["tecnologia"]);
  $fecha_inicio = json_encode($data["fecha_inicio"]);
  $fecha_fin = json_encode($data["fecha_fin"]);
  $robot = json_encode($data["robot"]);
  
  $sql_statement="INSERT INTO informes_robot (usuario, averia, fecha_creacion, accion_robot ,tipologia ,tecnologia ,fecha_inicio, fecha_fin, robot) VALUES ($usuario, $averia, $fecha_creacion, $accion_robot, $tipologia,$tecnologia,$fecha_inicio,$fecha_fin,$robot)";
  $verdadero = mysqli_query($conn,$sql_statement);
  if($verdadero){
    return 1;
  }else{
    return 0;
  }

  


}



function executephpexceladvh($data){

  //include "bbdd.php";
  include 'vendor/autoload.php';
  require_once('dbutils.php');
  $miConexion = conectarDB();


  $nombre = json_encode($data["nombre"]);
  
  

  $datosT = getInformeDB($miConexion);

  $excel = new Spreadsheet();
  $hojaActiva = $excel->getActiveSheet();
  $hojaActiva->setTitle('Informe');
  $hojaActiva->setCellValue('A1','ID');
  $hojaActiva->setCellValue('B1','USUARIO');
  $hojaActiva->setCellValue('C1','AVERIA');
  $hojaActiva->setCellValue('D1','FECHA_CREACION');
  $hojaActiva->setCellValue('E1','ACCION_ROBOT');
  $hojaActiva->setCellValue('F1','TIPOLOGIA');
  $hojaActiva->setCellValue('G1','TECNOLOGIA');
  $hojaActiva->setCellValue('H1','TIPO');
  $hojaActiva->setCellValue('I1','FECHA_FIN');
  $hojaActiva->setCellValue('J1','ROBOT');
  $hojaActiva->setCellValue('K1','FECHA_ESCALADO');

  $fila = 2;

  while($value = $datosT->fetch(PDO::FETCH_ASSOC)){  
  $hojaActiva->setCellValue('A'.$fila,$value['id']);
  $hojaActiva->setCellValue('B'.$fila,$value['usuario']); 
  $hojaActiva->setCellValue('C'.$fila,$value['averia']); 
  $hojaActiva->setCellValue('D'.$fila,$value['fecha_creacion']); 
  $hojaActiva->setCellValue('E'.$fila,$value['accion_robot']); 
  $hojaActiva->setCellValue('F'.$fila,$value['tipologia']); 
  $hojaActiva->setCellValue('G'.$fila,$value['tecnologia']); 
  $hojaActiva->setCellValue('H'.$fila,$value['fecha_inicio']); 
  $hojaActiva->setCellValue('I'.$fila,$value['fecha_fin']); 
  $hojaActiva->setCellValue('J'.$fila,$value['robot']); 
  $hojaActiva->setCellValue('K'.$fila,$value['fecha_escalado']); 
  $fila++;

  }


  $writer = IOFactory::createWriter($excel, 'Xls');
  $writer->save('./informes/informe.xls');
   return $nombre;
} 



function executephpcita($data){
  $nombre = json_encode($data["nombre"]);
    //TOKEN QUE NOS DA FACEBOOK
    $token = 'EAAbYoKIvTRMBAKIspKQGuSbyX9qrn5rD6pomgelhZBNhgJWKZCV8lwnpZBZA42ZC8ZADZBu00xJ0PmWi7J3WgzE8OBa0wrUvD26ySsTfeYR0LcQdZBmhzZBeE0UV7vPkPQcZCc6DRkDh12dges6PPP3yVLw3iMbrgvsyxJI3onQoW9nFLXCWmjJ9xLICQGp9ZC1Sy50JL3erppwSwZDZD';
    //NUESTRO TELEFONO
    $telefono = '34608285699';
    //URL A DONDE SE MANDARA EL MENSAJE
    $url = 'https://graph.facebook.com/v15.0/112697581749368/messages';

    //CONFIGURACION DEL MENSAJE
    $mensaje = ''
            . '{'
            . '"messaging_product": "whatsapp", '
            . '"to": "'.$telefono.'", '
            . '"type": "template", '
            . '"template": '
            . '{'
            . '     "name": "hello_world",'
            . '     "language":{ "code": "en_US" } '
            . '} '
            . '}';
    //DECLARAMOS LAS CABECERAS
    $header = array("Authorization: Bearer " . $token, "Content-Type: application/json",);
    //INICIAMOS EL CURL
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $mensaje);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //OBTENEMOS LA RESPUESTA DEL ENVIO DE INFORMACION
    $response = json_decode(curl_exec($curl), true);
    //IMPRIMIMOS LA RESPUESTA 
    print_r($response);
    //OBTENEMOS EL CODIGO DE LA RESPUESTA
    $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    //CERRAMOS EL CURL
    curl_close($curl);
  return $nombre;
}

function executephpexceladvh2($data){
  $nombre = json_encode($data["nombre"]);
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 2;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'cristian.medinayupanqui@zelenza.com';                     //SMTP username
        $mail->Password   = 'adlkzxtmflfyzrne';                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
        $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        //Recipients
        $mail->setFrom('cristian.medinayupanqui@gmail.com', 'ROBOT ADSL FTTH');
        $mail->addAddress('cristianmedinay@gmail.com');     //Add a recipient
      /*  $mail->addAddress('ellen@gmail.com');               //Name is optional
        $mail->addReplyTo('info@gmail.com', 'Information');
        $mail->addCC('cc@gmail.com');
        $mail->addBCC('bcc@gmail.com'); */

        //Attachments
        $mail->addAttachment('./informes/informe.xls');         //Add attachments
        $mail->addAttachment('./docs/grafica.pdf');
        //$mail->addAttachment('/docs/xampp.png', 'xampp.jpg');    //Optional name

        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = 'Informe Averias';
        $mail->Body    = 'Hola, buenos dias te adjunto el informe de hoy!</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
    return $nombre;
} 

function usuario($data){
    include "bbdd.php";
//if ($_SERVER['REQUEST_METHOD'] == 'GET')
  // {
    
    $nombre = json_encode($data["nombre"]);
    
   
    
    $sql = mysqli_query($conn,'SELECT * FROM usuarios WHERE nombre='.$nombre);
    while ($row = mysqli_fetch_assoc($sql))
     {
      $resultado =  json_encode($row);
     }
    

     // This is the important part.


// Some example function we want to run.
  sleep(5);

/*   $command = shell_exec('python C:\xampp\htdocs\Proyecto\pyautogui_imp.py');
  echo $command;  */
 
     return $resultado;
 //   }
} 

function usuarios($data){
  include "bbdd.php";
  $nombre = json_encode($data["nombre"]);
  $sql = mysqli_query($conn,'SELECT * FROM usuarios');
  $saco = array();
  while ($row =  mysqli_fetch_array($sql))
     {
        $fruit = array (
           "nombre"  => $row['nombre'],
           "password" => $row['password'],
       );
       array_push($saco,$fruit);
     }
     
     echo json_encode($saco);
} 

function delayrand($data){
  include "bbdd.php";
  $nombre = json_encode($data["nombre"]);


  $sql = mysqli_query($conn,'SELECT * FROM delay_aleatorio');
  $saco = array();
  while ($row =  mysqli_fetch_array($sql))
     {
        $fruit = array (
           "numero_aleatorio"  => $row['numero_aleatorio'],
           
       );
       array_push($saco,$fruit);
     }
     
     echo json_encode($saco);


} 

?>


